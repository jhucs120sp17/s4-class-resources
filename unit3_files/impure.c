#include <stdio.h>
#define SIZE 20

int arr[SIZE] = {0};

void init_fib() {
    for (int i=2; i < SIZE ; i++) {
       arr[i] = arr[i-2] + arr[i-1];
    }
}
void print_fib(int x) {
    printf("x=%d\n", arr[x]);
}
int main() {
    arr[1] = 1;
    print_fib(10);
    return 0;
}
