#include <stdio.h>

void print_inc_x() {
  static int x = 3;
  printf("x=%d ", x);
  x++;
}

int main() {
  for (int i=0; i<4; i++) {
    print_inc_x();
  }
  return 0;
}
