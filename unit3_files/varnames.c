#include <stdio.h>

void print_x(int x) {
  printf("x=%d\n", x);
}

int main() {
    int x = 3; // local to this block
    for (int i=0; i<4; i++)
        print_x(x++);
    float x = 3.4; // ???
    return 0;
}
