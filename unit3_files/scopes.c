int g = 10;
// global scope

void foo(int x, float y) {
    // local function scope
    if (x == 3) {
        // local block scope 1
        for (int i=0; i<4; i++) {
            // local block scope 2
            x = 11;
        }
    }
}
int main() { foo(1, 1.1); }
