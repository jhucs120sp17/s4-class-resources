#include <stdio.h>

// There are many errors in this file. Use GDB to find them.
const int SIZE = 200;

int* copy_arr(int src[], int len) {
    int dst[5];
    for (int i=0; i<=len; ++i) {
        dst[i] = src[i];
    }
}

void copy_str(char dst[], char src[]) {
    int i;
    while (src[i]) {
        dst[i] = src[i];
    }
}

void test_str() {
    char str[] = {'h','e','l','l','o',' ','w','o','r','l','d'};
    char str2[5];
    char str3[11];
    char str4[] = "gdb is great!";

    copy_str(str2, str);
    copy_str(str3, str);

    puts(str);
    puts(str2);
    puts(str3);

    str4[0] = 'G';
    str4[4] = 'I';
    str4[7] = 'G';
    puts(str4);
}

void test_arr() {
    int arr[] = {1, 3, 10, 2, 0};

    int* arr2 = copy_arr(arr, sizeof(arr)/sizeof(arr[0]));

    for(int i=0; i<12; i++) {
        printf("%d ", arr2[i]);
    }
    puts("");
}

int main() {
    test_str();
    test_arr();
}
