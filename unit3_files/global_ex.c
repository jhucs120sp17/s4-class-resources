#include <stdio.h>

int x = 3;

void print_x() {
    printf("x=%d\n", x);
}

int main() {
    for (int i=0; i<4; i++) {
        print_x();
        x++;
    }
    return 0;
}
