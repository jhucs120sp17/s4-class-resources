#include <stdio.h>

void assign(int x) {
  x = 4;
  printf("assign: x = %d\n", x);
}

int main() {
  int x = 2; // declared here
  assign(x);
  printf("main: x = %d\n", x);
}
