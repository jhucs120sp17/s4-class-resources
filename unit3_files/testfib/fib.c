#include <stdio.h>
#include "fib.h"
int fib(int a) {
    if (a == 0) return 0;
    else if (a == 1) return 1;
    else return (fib(a-1) + fib(a-2));
}
int test_fib() {
    if (fib(1) != 1) return 0;
    if (fib(0) != 0) return 0;
    // Exercise: add more cases
    return 1;
}
