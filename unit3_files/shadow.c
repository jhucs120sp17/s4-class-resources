#include <stdio.h>

int x = 5; // global

void foo() {
  printf("foo start: x=%d\n", x);
  int x = 2; // local
  if (x > 0) {
     static int x = 1; // static
     printf("in if: x=%d\n", x);
  }
  printf("in foo: x=%d\n", x);
}

int main() {
  printf("main start: x=%d\n", x);
  x = 3;
  foo();
  printf("main end: x=%d\n", x);
  return 0;
}
