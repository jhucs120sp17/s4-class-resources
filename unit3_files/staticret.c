#include <stdio.h>

char* print_x(int x) {
  static char s[30]; // local
  sprintf(s, "x=%d\n", x);
  return s;
}

int main() {
    int x = 3; // local to this block
    for (int i=0; i<4; i++)
        puts(print_x(x++));
    return 0;
}
