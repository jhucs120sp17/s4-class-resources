#include <stdio.h>
void print_dec(int num) {
    if (num == 0) { return; }
    else {
        printf("%d\n", num);
        print_dec(num-1);
    }
}
int main() {
   int x = getchar();
   print_dec(x);
   return 0;
}
