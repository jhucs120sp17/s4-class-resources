#include <stdio.h>

void print_x(int x) {
  char s[30]; // local
  printf("x=%d\n", x);
}

int main() {
  {
    int x = 3; // local to this block
    for (int i=0; i<4; i++)
      print_x(x++);
  }
  return x; // ???
}
