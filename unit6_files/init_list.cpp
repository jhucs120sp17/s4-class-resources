#include <cstdio>
struct Bar {
    Bar(int y=3) : m_y(y), m_z(4.5) {
        std::puts("Bar constructor");
    }
    int m_y;
    float m_z;
};
struct Foo {
    Foo() : m_bar(12), m_x(2) {
        puts("Foo constructor");
    }
    Bar m_bar;
    int m_x;
};
int main() {
    Foo f;
}
