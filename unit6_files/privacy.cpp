#include <cstdio>
class Foo {
    void foo() {}
    void foo3(Foo& f) {
        f.m_x = 3;
    }
private:
    void foo2() {}
    int m_x = 2;
};
class Bar {
public:
    void bar(class Foo& f) {
        f.foo();
        f.foo2();
        std::printf("f.m_x = %d\n", f.m_x);
    }
};
int main() {
    Foo f, g;
    Bar b;
    f.foo();
    f.m_x = 5;
    Bar.bar(f);
}
