#include <iostream>
class Foo {
public:
    Foo(int x) : m_x(x) {}
    std::ostream& operator<<(const Foo& f) const {
        // Implement this
    }
    int m_x;
};
int main() {
    Foo g(5);
    std::cout << g;
}
