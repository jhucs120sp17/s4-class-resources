class Foo {
public:
    Foo(int x) : m_x(x) {}
    // Why are we returning anything?
    Foo& operator=(const Foo& f) {
        // Implement this
    }
    int m_x;
};
int main() {
    Foo g(5), h = 2; // Regular constructor
    g = h; // Assignment operator
}
