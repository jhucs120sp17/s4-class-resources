struct A {
    int x;
    A() { x=1; } // explicit default constructor
    A(const A& a) = default; // implicit copy constructor
};
struct B {
    int y;
    B(const B& b) { y = b.y; } // explicit copy constructor
    // no default constructor since constructor specified
};
struct C {
    int z;
    C(const C& c) = delete;
    C() = default;
};
int main() {
    A a;
    // B b; // not possible
    C c; 
    A a2(a);
    //B b2(b);
    // C c2(c); // not possible
}
