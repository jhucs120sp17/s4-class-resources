#include <sstream>
#include <iostream>

void foo() {
    std::stringstream ss("hello");
    std::cout << ss.str();
    int x = 5; float y = 3.4;
    ss << x << y;
    std::cout << ss.str();
}
int main() {
    foo();
}
