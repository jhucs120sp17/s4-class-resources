#include <cstdio>
class Foo {
public:
    Foo(int x) {
        m_x = x;
        std::puts("Foo converting constructor");
    }
    int m_x;
};
void bar(Foo f) {
    printf("Received a Foo from main of value %d\n", f.m_x);
}
int main() {
    bar(4); // How can this work?
}
