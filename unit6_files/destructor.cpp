#include <cstdlib>
#include <cstdio>
struct Bar {
    int m_y;
};
struct Foo {
    Foo() {
        m_x = static_cast<int *>(malloc(sizeof(int)));
        m_bar = static_cast<Bar *>(malloc(sizeof(Bar)));
        std::puts("default constructor");
    }
    ~Foo() {
        // Exercise: What to do here?
        puts("destructor");
    }
    int *m_x;
    Bar *m_bar;
};
int main() {
    Foo f;
}
