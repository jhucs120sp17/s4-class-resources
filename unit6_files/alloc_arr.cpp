#include <cstdio>
struct Foo {
    Foo() : m_x(0) {
        std::puts("Foo()");
    }
    Foo(int x) : m_x(x) {
        std::puts("Foo(int)");
    }
    ~Foo() {
        std::puts("~Foo()");
    }
    int m_x;
};
int main() {
    Foo* f = new Foo[12]; // Can't pass args
    for (int i=0; i<12; i++) {
        f[i] = Foo(12); // Notice no 'new'!!! Why?
    }
    delete [] f;
}
