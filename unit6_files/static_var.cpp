#include <cstdio>
class Foo {
public:
    void foo() {
        std::printf("x=%d\n", this->m_x); // ???
        std::printf("x=%d\n", m_x);
        std::printf("y=%f\n", m_y); // accessed via this
        std::printf("y=%f\n", this->m_y);
    }
private:
    static int m_x;
    float m_y;
};

int Foo::mX = 3; // initialize here like a global

int main() {
    Foo f;
    f.foo();
    // Exercise: assign a value to m_x and m_y in a function
    // Exercise: create an array of Foo's
    // Exercise: call this function in a loop and check the result
    // What's the result?
}
