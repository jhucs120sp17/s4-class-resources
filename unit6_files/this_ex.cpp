#include <cstdio>
class Foo {
public:
    Foo* foo() {
        std::printf("%d", this->m_x);
        return this;  // Notice that this is a pointer
    }
private:
    int m_x = 2;
};
int main() {
    // Create Foo
    // Print the address of Foo
    // call its foo function
    // Print the return pointer
}
