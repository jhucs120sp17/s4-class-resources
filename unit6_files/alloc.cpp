#include <cstdio>
struct Foo {
    Foo() : m_x(0) {
        std::puts("Foo()");
    }
    Foo(int x) : m_x(x) {
        std::puts("Foo(int)");
    }
    ~Foo() {
        std::puts("~Foo()");
    }
    int m_x;
};
int main() {
    Foo* f = new Foo();   // allocate Foo
    Foo* g = new Foo(12); // can pass arguments
    int* x = new int;     // also works!
    delete x;             // don't forget
    delete g;
    delete f;
}
