#include <cstdio>
class Foo {
public:
    static void foo() {
        std::printf("m_x=%d\n", this->m_x);
        std::printf("m_y=%d\n", m_y);
    }
private:
    static int m_x;
    float m_y;
};

int Foo::m_x = 3;

int main() {
    Foo f;
    f.foo(); // OK even though static
    Foo::foo(); // Also OK
    // Exercise: add instance method bar() which calls foo()
    // Exercise: add static method baz() which calls foo()
}
