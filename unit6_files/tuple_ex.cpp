#include <tuple>
#include <iostream>
#include <string>

// We can finally return more than one value!
std::tuple<int, std::string, char> foo(int x) {
    if (x == 5) {
        // Note cool way of creating pairs!
        return {0, "hello", 'c'};
    } else {
        // More traditional pair creation
        return std::make_tuple(1, "goodbye", 'a');
    }
}

int main() {
    std::tuple<int, std::string, char> t = foo(5);
    std::cout << std::get<0>(t) << ", " << std::get<1>(t) <<
        ", " << std::get<2>(t) << '\n';
}
