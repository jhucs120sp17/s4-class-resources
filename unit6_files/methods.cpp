#include <cstdio>
    class Foo {
    public:
        void foo() {
            std::printf("%d", this->m_x);
            m_x = 5; // Uses implicit this ptr
            std::printf("%d", m_x); // Preferred
        }
    private:
        int m_x = 2;
    };
int main() {
    Foo f;
    f.foo();
}
