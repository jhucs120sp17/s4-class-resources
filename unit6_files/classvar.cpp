#include <cstdio>
class Foo {
public:
    int m_x;
    float m_y;
    char m_c;
};
int main() {
    Foo f;
    Foo* pf = &f;
    char& rc = f.m_c;
    f.m_x = 3;
    pf->m_y = 1.5;
    rc = 'a';
    std::printf("f.x=%d, f.y=%f, f.c=%c\n", f.m_x, f.m_y, f.m_c);
}
