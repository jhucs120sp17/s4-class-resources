// Find which parts here work
struct Foo {
    struct Bar {
        void bar(Foo& f) {
            x = 3;
            f.x++;
        }
    private:
        int y;
    };
    void foo(Bar &b) {
        y++;
        b.y++;
    }
private:
    int x;
};
int main() {
    Foo f;
    Foo::Bar b;
}
