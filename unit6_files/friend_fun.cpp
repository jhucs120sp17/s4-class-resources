#include <cstdio>
class Bar;
class Foo {
public:
    void foo(Bar& b);
    void foobar(Bar& b);
};
class Bar {
    // Mark the necessary functions as friends
private:
    int m_x;
};
void baz(Bar &b) {
    std::printf("b.m_x=%d\n", b.m_x);
}
void Foo::foo(Bar& b) {
    b.m_x = 3; // OK!
}
void Foo::foobar(Bar& b) {
    b.m_x = 12; // Not OK!
}
int main() {
    Foo f;
    Bar b;
    f.foo(b);
    baz(b);
}
