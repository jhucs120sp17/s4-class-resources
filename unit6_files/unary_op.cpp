class Foo {
public:
    Foo operator~() const {
        Foo f(*this);
        f.m_x ^= f.m_x;
        return f;
    }
    int m_x = 43;
};
int main() {
    Foo g;
    Foo h = ~g;
}
