#ifndef CONST_H
#define CONST_H
class Foo {
public:
    void printArray();
    void randArray();
    void set_x(int x) { m_x = x; }
    int get_x() { return m_x; }
private:
    int m_values[5] = {1, 2, 3, 4, 5};
    int m_size = 5;
    int m_x;
};
#endif
