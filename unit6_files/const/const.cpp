#include <cstdio>
#include <cstdlib>
#include "const.h"
void Foo::printArray() {
    for (int i=0; i<m_size; i++) {
       std::printf("%d ", m_values[i]);
    }
}
void Foo::randArray() {
    for (int i=0; i<m_size; i++) {
        m_values[i] = rand();
    }
}
int main() {
    Foo f;
    f.randArray();
    f.printArray();
    f.set_x(5);
    int x = f.get_x();
}
