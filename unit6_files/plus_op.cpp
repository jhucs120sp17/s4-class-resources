class Foo {
public:
    Foo(int x) : m_x(x) {}
    Foo operator+(const Foo& f) const {
        // Implement + as *
    }
    int m_x;
};
int main() {
    Foo g(5), h(2);
    Foo i = g + h;
}
