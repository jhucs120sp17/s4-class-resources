#include <fstream>
#include <iostream>
int main() {
    std::ofstream f("file.txt");
    if (f.is_open()) {
        f << 5 << " world" << '\n';
    }
    f.close();
    std::ifstream ifs;
    ifs.open("file.txt");
    int x = 0;
    std::string s;
    if (ifs.is_open()) {
        ifs >> x >> s;
    }
    ifs.close();
    std::cout << x;
}
