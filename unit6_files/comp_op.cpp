#include <iostream>
class Foo {
public:
    Foo() : m_x(0) {}
    Foo(int x) : m_x(x) {}
    bool operator==(const Foo& f) const {
        // Implement this
    }
    int m_x;
};
int main() {
    Foo g(2), h(2);
    std::cout << (g == h) << "\n";
}
