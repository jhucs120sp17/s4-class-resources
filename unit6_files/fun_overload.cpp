#include <string>
int x;
char* c;
char h[] = "hello";
void foo(int y) {
    x = y;
}
int foo(int y, int z) {
    x = y + z;
    return x;
}
char foo(char *s) {
    c = s;
    return 'a';
}
void foo() {
    x = 3;
}
int main() {
    foo(1);
    foo(2,3);
    foo(h);
    foo();
}
