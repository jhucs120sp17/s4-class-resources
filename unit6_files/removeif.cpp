#include <list>
#include <iostream>
using namespace std;

bool singleDigit(const int& value) { return (value < 10); }
void print(list<int> mylist) {
    for (list<int>::iterator it=mylist.begin(); it != mylist.end(); ++it) {
        cout << *it << ", ";
    }
    cout << endl;
}
int main() {
    list<int> mylist = {20, 100, 3, 40, 8};
    mylist.remove_if(singleDigit);
    print(mylist);
    // sort the list
    print(mylist);
}
