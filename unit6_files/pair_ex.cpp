#include <utility>
#include <string>
#include <iostream>

// We can finally return more than one value!
std::pair<int, std::string> foo(int x) {
    if (x == 5) {
        // Note cool way of creating pairs!
        return {0, "hello"};
    } else {
        // More traditional pair creation
        return std::make_pair(1, "goodbye");
    }
}

int main() {
    std::pair<int, std::string> p = foo(5);
    std::cout << p.first << ", " << p.second << '\n';
}
