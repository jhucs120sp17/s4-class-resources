#include <cstdio>
class Foo {
public:
    Foo() {
        std::puts("Presto Constructo!");
    }
    ~Foo() {
        std::puts("Presto Destructo!");
    }
};
int main() {
    Foo f;  // Nothing in our sleeves
}
