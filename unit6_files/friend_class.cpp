class Foo; // Note the early declaration
class Bar {
    friend class Foo;
public:
    void bar(Foo& f) {
        // Exercise: Set m_y to 3
    }
private:
    int m_x = 2;
};
class Foo {
public:
    void foo(Bar& b) {
        // Exercise: Set m_x to 3
    }
private:
    int m_y = 1;
};
int main() {
    Foo f;
    Bar b;
    f.foo(b);
    b.bar(f);
}
