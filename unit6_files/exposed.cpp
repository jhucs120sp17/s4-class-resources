#include <cstdio>
class ArrayPrinter {
public:
    void print() {
        bool first = true;
        for (int i=0; i<m_size; i++) {
            std::printf("%s%d", first ? "" : " ", m_arr[i]);
            first = false;
        }
    }
    int* m_arr = NULL;
    int m_size = 0;
};
int main() {
    ArrayPrinter ap;
    int arr[] = {1,2,3,4};
    ap.m_arr = arr;
    ap.m_size = -4;
    ap.print();
}
