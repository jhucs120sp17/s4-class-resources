#include <cstdio>
class Foo {
public:
    Foo() {
        m_x = 6;
        std::puts("default constructor");
    }
    int m_x = 2;
};
int main() {
    Foo f;
    printf("f.x = %d\n", f.m_x);
}
