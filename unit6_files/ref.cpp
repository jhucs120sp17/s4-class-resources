#include <cstdio>

using namespace std;

void foo(int value, int* pointer, int& alias) {
    value++;
    *pointer = *pointer + 5;
    alias = alias + 10;
}
int main() {
    int a = 10, b = 10, c = 10;
    foo(a, &b, c);          // 2nd and 3rd parameters having same effect
    printf("%d %d %d\n", a, b, c);

    int &d = c;      // d is now a reference to (alias for) c
    d = 30;
    printf("%d, %d\n", c, d);
    // don't need main to return 0 in c++
}
