int x;
int foo() {
    x = 3;
    return 0;
}
void foo() {
    x = 5;
}
char foo() {
    x = 7;
    return 'a';
}
int main() {
    int x = foo();
    foo();
    char c = foo();
}
