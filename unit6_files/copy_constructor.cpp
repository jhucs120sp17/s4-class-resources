#include <cstdio>
using namespace std;
struct Foo {
    Foo() = default; // default constructor
    Foo(const Foo&) { // Why &?
        std::puts("copy constructor");
    }
};
// global function
Foo bar(Foo) {   // Return
    puts("in bar");
    Foo g;
    return g;
}
int main() {
    Foo f1;
    puts("here1");
    Foo f2(f1);
    puts("here2");
    Foo h(bar(f1));
    puts("here3");
}
