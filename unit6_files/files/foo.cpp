#include "foo.h"
#include <cstdio>
bool Foo::foo(int x) {
    std::printf("foo %d\n", x);
    m_y = 3;
    return true;
}

void Foo::bar() {
    std::printf("bar %d\n", m_x);
}
int main() {
    Foo f;
    f.foo(1);
    f.bar();
}
