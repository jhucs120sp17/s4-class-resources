#include <iostream>
class Foo {
public:
    Foo() : m_x(0) {}
    Foo(int x) : m_x(x) {}
    Foo& operator++() {
        // Implement this
    }
    Foo operator++(int) {
        // Implement this
    }
    int m_x;
};
int main() {
    Foo g(2), h(2);
    g++; ++h;
}
