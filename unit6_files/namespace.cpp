#include <cstdio>

using std::puts;

namespace x {
   void foo() {
       std::puts("x::foo()");
   }
   namespace y {
       void foo() {
         puts("x::y::foo()");
       }
   }
}
namespace z {
   void foo() {
       puts("z::foo()");
   }
}
void foo() {
    std::puts("::foo()");
}

// main must be in the global namespace
int main() {
    // call each one of the foo functions
}
