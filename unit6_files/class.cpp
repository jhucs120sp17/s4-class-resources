#include <cstdio>
class Foo {
  public:
    Foo() { m_x = 4; }
    void print() {
        std::printf("m_x = %d", m_x);
    }

  private:
    int m_x; // note the m prefix for 'member'

}; // watch out for the semicolon like struct
int main() {
    Foo f;  // no need to write 'class Foo', unlike C struct
    f.print();
}
