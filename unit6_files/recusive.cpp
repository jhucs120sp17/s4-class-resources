struct Foo {
    int m_y;
    void foo(Bar& b) {
        b.m_x = 3; // Need to know Bar's structure
    }
};
struct Bar {
    int m_x;
    void bar(Foo& f) {
        f.m_y = 4;
    }
};
int main() {
    Foo f;
    Bar b;
    f.foo(b);
    b.bar(f);
}
