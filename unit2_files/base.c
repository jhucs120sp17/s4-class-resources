#include <stdio.h>

void printNum(int num) {
    printf("decimal: %?\n", num);
    printf("octal: %?\n", num);
    printf("hex: %?\n", num);
    printf("binary: %?\n", num);
}
int main(int argc, char** argv) {
    int num = atoi(argv[1]);
    printNum(num);
    return 0;
}
