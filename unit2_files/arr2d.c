    void bar(int arr_inner[]) {
        arr_inner[1] = 10;
    }
    void foo(int arr[][3]) {
        bar(arr[2]);
    }
    int main() {
        int arr[4][3] = {{0}};
        foo(arr);
        return 0;
    }
