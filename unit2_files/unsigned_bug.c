#include <stdio.h>
void foo() {
    int arr[5] = {1, 2, 3, 4, 5};
    for (unsigned long i=sizeof(arr); i>=0; i--) {
       printf("%d ", arr[i]);
    }
}

int main() {
    foo();
    return 0;
}
