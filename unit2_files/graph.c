#include <stdio.h>
#include <math.h>

#define DIMX 80
#define DIMY 20
#define YOFFSET (DIMY / 2)
#define PI 3.14159265359
#define FUN(x) sin(x)

// Print the screen
void print(char screen[]) {
  for (int y=(DIMY - 1) * DIMX; y >= 0; y -= DIMX) {
    for (int x=0; x < DIMX; x++) {
      if (screen[x + y * DIMX]) {
        putchar('x');
      } else {
        putchar(' ');
      }
    }
    putchar('\n');
  }
}
void paint(char screen[]) {
  double xdelta = 2 * PI / (1 * DIMX);
  double ydelta = 2.0 / DIMY;
  for (int x=0; x < DIMX; x++) {
    double v = FUN(xdelta * x);
    for (int y=0; y < DIMY; y++) {
      // what goes here?
    }
  }
}
int main() {
  char screen[DIMY * DIMX] = {0};
  paint(screen);
  print(screen);
  return 0;
}
