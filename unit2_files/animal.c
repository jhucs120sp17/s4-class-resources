#include <stdio.h>
#include <assert.h>
#define PAWS   0x1
#define WINGS  0x2
#define FUR    0x4
#define EGGS   0x8
#define BILL   0x10
#define ALL (PAWS | WINGS | FUR | EGGS | BILL)
#define NONE (~ALL)
void test(int choice) {
  assert((choice & ???) == 0);  // No errant bits
  if (choice & ???) {
    puts("Are you a dog?\n");
  } else if (choice & ???) {
    puts("Are you a duck?\n");
  } else if (choice & ???) {
    puts("Are you an animal?\n");
  } else puts("I give up.\n");
}
int main() {
  puts("Enter a hex number: ");
  int c;
  scanf("%x", &c);
  test(c);
  return 0;
}
