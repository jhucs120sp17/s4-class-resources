#include <stdio.h>
void foo() {
    int arr[50] = {1};
    int *ptr = arr;

    // 2 methods to use arithmetic here:
    for (int i=0; i<50; i++) {
        printf("%d ", *ptr);
        ++ptr; // increment by 4 bytes
    }
    for (int i=0; i<50; i++) {
        printf("%d ", *(ptr + i));
    }
}
int main() {
    foo();
    return 0;
}
