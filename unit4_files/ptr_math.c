#include <stdio.h>
void foo() {
    int arr[10];
    int *p = arr;
    printf("before %p\n", (void *)p); // print the pointer
    p += 5; // How much will this add?
    printf("after %p\n", (void *)p);

    // modify element 5 of the array
    // modify element 2 of the array with the same pointer
    // now subtract 3 from the array. What do you see?
    // now loop over the array with a for loop and fill each element with 2
}
int main() {
    foo();
    return 0;
}
