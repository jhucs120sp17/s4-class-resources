#include <stdio.h>
#include <stdlib.h>
void get_random(int num, int* arr) {
    for (int i=0; i<num; i++) {
        arr[i] = rand();
    }
}
void print_ints(int num, int* arr) {
    for (int i=0; i<num; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}
int main() {
    int num1, num2;
    puts("how many numbers to generate?");
    scanf("%d", &num1);
    int* arr = calloc(num1 * sizeof(int));
    if (arr == NULL) exit(1); // good to check
    printf("%p\n", (void *)arr);
    gen_random(num1, arr);
    print_ints(num1, arr);
    puts("how many more numbers to generate?");
    scanf("%d", &num2);
    if (num2 < 0) return 0;
    int total = num1 + num2;
    int* arr = realloc(total * sizeof(int));
    printf("%p\n", (void *)arr); // compare to above
    gen_random(total, arr);
    print_ints(total, arr);
    return 0;
}
