#include <stdio.h>
#include <stdlib.h>

void foo(short arr[]) { // Try adding a size in []
    printf("in foo: sizeof(arr)=%lu\n", sizeof(arr));
    // Try changing arr itself
}
int main() {
    short arr[20];
    short* parr = arr; // make parr point to arr
    printf("in main: sizeof(arr)=%lu\n", sizeof(arr));
    printf("in main: sizeof(parr)=%lu\n", sizeof(parr));
    foo(arr);
}
