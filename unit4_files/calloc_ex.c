#include <stdio.h>
#include <stdlib.h>
void gen_random(int num, int* arr) {
    for (int i=0; i<num; i++) {
        arr[i] = rand();
    }
}
void print_ints(int num, int* arr) {
    for (int i=0; i<num; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}
int main() {
    int num;
    puts("how many numbers to generate?");
    scanf("%d", &num);  // what if > 5?
    int arr[5] = {0};
    // change to dynamic memory
    gen_random(num, arr);
    print_ints(num, arr);
    return 0;
}
