#include <stdio.h>
#include <stdlib.h>

int main() {
    float* farr = malloc(sizeof(float) * 100);
    int* iarr = calloc(20, sizeof(int));

    for (int i=0; i<100; i++) {
        *(farr + i) = 2.5;
    }
    for (int i=0; i<20; i++) {
        iarr[i] = 45;
    }
    iarr = 0;
    return 0;
}
