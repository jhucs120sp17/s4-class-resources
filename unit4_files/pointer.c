#include <stdio.h>

void foo() {
    int x = 15, y = 2;
    int *px, *py;  // Notice the syntax here

    // Modify ptr right now. Is this a good idea?
    // Make px point to 5. Does this work?
    // Make px point to x, py point to y
    // Print out the value of px and x
    // Add 17 to y using py
    // Make px point to y
    // Modify y via px and check what py sees
    // Try to add 1 to px and see what you find there. What is that?
    // Put 0 in px and try to read through it
}
int main() {
    foo();
    return 0;
}
