#include <stdio.h>
void foo(int* const p) {
    ++(*p);
    printf("%d\n", *p);
}
void bar(const int* p) {
    ++p;
    printf("%d\n", *p);
}
void baz(const int* const p) {
    printf("%d\n", *p);
}
void bam(int* p) {
    ++(*p);
    ++p;
    printf("%d\n", *p);
}
int main() {
    int arr[6] = {0};
    foo(arr); bar(arr); baz(arr); bam(arr);
    return 0;
}

