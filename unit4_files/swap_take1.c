#include <stdio.h>

int swap(int x, int y) {
    x = y;
    // y = ???;
    return x; // we're limited to one return value
}
int main() {
    int x = 2, y = 4;
    x = swap(x, y);
    printf("x is %d, y is %d\n", x, y);
}
