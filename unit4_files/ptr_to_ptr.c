#include <stdio.h>
// Fix this program to make it work

// Return a pointer to largest and its value
int find_largest(int* start, int* end, int* result) {
    int* largest = NULL, *ptr = start;
    while (ptr++ != end) {
        if (largest && *ptr > *largest) {
            largest = ptr;
        }
    }
    result = largest;
    return *largest;
}
int main() {
    int nums[] = {4, 5, 104, 2};
    int *largest = NULL;
    int len = find_largest(nums, nums + 4, largest);
    printf("largest: %d\n", *largest);
    return 0;
}
