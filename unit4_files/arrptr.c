#include <stdio.h>

// Notice how sp becomes a ptr-to-ptr
void print(char **sp, int len) {
    for (int i=0; i<len; i++) {
        printf("%s ", *sp);
    }
}

void foo() {
    char *sp[4] = {"Hearts", "Diamonds",
        "Clubs", "Spades"};
    char sa[4][20] = {"hearts", "diamonds",
        "clubs", "spades"};
    printf("%s %s\n", sp[1], sa[1]); // similar
    printf("sizeof(sp) = %lu sizeof(sa) = %lu\n",
      sizeof(sp), sizeof(sa)); // different
    print(sp, 4);
}
int main() {
    foo();
    return 0;
}
