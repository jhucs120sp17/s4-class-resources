#include <stdio.h>
void foo(int* p) {
    ++(*p);
    printf("%d\n", *p);
}
void bar(int* p) {
    ++p;
    printf("%d\n", *p);
}
void baz(int* p) {
    printf("%d\n", *p);
}
void bam(int* p) {
    ++(*p);
    ++p;
    printf("%d\n", *p);
}
int main() {
    int arr[6] = {0};
    foo(arr); bar(arr); baz(arr); bam(arr);
    return 0;
}
