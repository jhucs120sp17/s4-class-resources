#include <stdio.h>
#include <stdlib.h>
struct Person {
    char *first;
    char *last;
    int age;
};

char* firsts[] = {"John", "Bob", "Bill", "Lisa"};
char* lasts[] = {"Smith", "Dole", "Murray", "Hill"};

// Fix these functions!
void initPerson(struct Person p, int i) {
    p.first = firsts[i];
    p.last = lasts[i];
    p.age = rand() % 50;
}
void printPerson(struct Person p) {
    printf("%s %s is %d years old\n",
        p.first, p.last, p.age);
}
int main() {
    // Allocate an array of Persons
    struct Person* people =
        malloc(sizeof(struct Person) * 4);
    // Initialize the people
    for (int i=0; i<4; i++) {
        initPerson(people[i], i);
    }
    // Print the details
    for (int i=0; i<4; i++) {
        printPerson(people[i]);
    }
}
