#include <stdio.h>
#include <stdlib.h>

// comparison function
int compare(const void* x, const void* y) {
    return *(int *)x - *(int *)y;
}
void print_arr(int size, int* arr) {
    for (int i=0; i < size; ++i) {
        printf("%d ", arr[i]);
    }
    puts("");
}
void foo() {
    int values[] = {100, 4, 24, 33, 8};
    int len = sizeof(values) / sizeof(values[0]);
    print_arr(len, values);
    // C library quicksort function:
    // void qsort(void* base, size_t num, size_t size,
    //    int (*compar)(const void*, const void*))
    // Use quicksort here with our function
    print_arr(len, values);
}
int main() {
    foo();
    return 0;
}
