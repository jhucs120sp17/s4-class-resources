#include <stdio.h>
// fix this
#define SUM(x, y) x + y
#define READ(p) *p

void foo() {
    int arr[] = {1, 5, 12};
    int* p = arr;

    printf("%d\n", SUM(arr[1], arr[2]));
    printf("%d\n", SUM(arr[1], arr[2]) * 5);
    printf("%d\n", ++READ(p));
}
int main() {
    foo();
    return 0;
}
