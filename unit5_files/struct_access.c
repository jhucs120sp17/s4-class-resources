#include <stdio.h>
#include <string.h>
struct Person {
    char *first;
    char *last;
    int ssn[9];
    int age;
};

int main() {
    struct Person john;
    char last[] = "Smith";
    john.first = "John"; // where is this pointing?
    john.last = last; // and where is this pointing?
    int ssn[] = {3,4,2,4,5,3,2,4,7};
    memcpy(john.ssn, ssn, sizeof(ssn));
    john.age = 33;
    john.age++;
    printf("%s's age is %d\n", john.first, john.age);
}
