#include <stdio.h>
struct Person {
    char *first;
    char *last;
    int ssn[9];
    int age;
};

int main() {
    // By name. Any unfilled fields are set to 0
    struct Person john =
      {.first="John", .last="Smith"};
    // By position:
    struct Person bob =
      {"Bob", "Dole", {9,4,2,4,5,1,2,0,7}, 50};
    //  Note that like arrays, initialization is only available
    //  at the time we declare the struct.
    printf("%s's age is %d\n", john.first, john.age);
}
