struct Person {
    char *first;
    char *last;
    int ssn[9];
    int age;
}; // Notice the semicolon!!!

int main() {
    // Notice struct keyword before person
    struct Person per1, per2;
}
