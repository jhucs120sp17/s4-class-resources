#include <stdio.h>
struct Name {
    char first[30];
    char last[30];
};
struct Person {
    struct Name name;
    int ssn[9];
    int age;
};

int main() {
    struct Person john =
      {.name={"John", "Smith"}, .age=33};
    printf("%s's age is %d\n", john.name.first, john.age);
}
