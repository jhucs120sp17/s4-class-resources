struct Name {
    char first[30];
    char last[30];
}
struct Person {
    struct Name name;
    int ssn[9];
    int age;
};

int main() {
    struct Person john = {.name={"John", "Smith"}, age=33};
    struct Person *pp = &john;
    printf("%s's age is %d\n", pp.name.first, pp.age); //?
}
