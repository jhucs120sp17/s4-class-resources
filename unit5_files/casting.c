#include <stdio.h>
void foo() {
    int i = 45;
    printf("float %f\n", (double)i);
    printf("char %c\n", (char)i);

    // Sometimes useful (but very dangerous)
    int *ptr = (int *) 2398438; // ?
    i = *ptr;
}
int main() {
    foo();
    return 0;
}
