#include <stdio.h>

// Social security number
struct Social {
    int n1, n2, n3;
};
void readSocial(/*ssn*/) {
    // Use scanf to fill the ssn struct. Read 123-45-6789 format
}
void printSocial(/*ssn*/) {
    // Use printf to print the ssn out in proper format
}
void newSocial(/*ssn*/) {
    // increment last number of ssn by 1
}
int main() {
    // ssn = allocate an ssn struct
    readSocial(/*ssn*/);
    newSocial(/*ssn*/);
    printSocial(/*ssn*/);
    // free the ssn
    return 0;
}
