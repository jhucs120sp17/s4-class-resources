#include <stdlib.h>
#include "list.h"

// Assumes a sorted list
void removeSorted(struct Node** head, int val) {
  struct Node* p = *head;
  struct Node* q = p;

  while(p != NULL && p->data < val) {
    q = p;
    p = p->next;
  }

  if (p == NULL || p->data > val) {
     // We didn't find val.
     // We may want to return an error
    return;
  }
  // p must be a match
  if (p == *head) {
    // first element
    // fill this in
  } else {
    // fill this in
  }
  // Mustn't forget to do something here...
}
