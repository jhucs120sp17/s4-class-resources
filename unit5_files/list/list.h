#ifndef LIST_H
#define LIST_H
struct Node {
  int data;
  struct Node * next;
};

/* Generic Int List Interface */

// count and return the number of elements in the List
int size(struct Node*);
// print the list iteratively
void printList(struct Node*);
// print the list in reverse
// void printReverse(struct Node *);

// Don't use any of these on sorted lists:
//    they'll ruin the order
// add int to beginning of list
void pushFront(struct Node **, int);
// add int to end of list
void pushBack(struct Node **, int);
// struct Node* insertAtIdx(struct Node **,
//    int val, int idx);

// delete int from list if it is there
// inefficient on sorted list
// void removeByVal(struct Node **, int val);
// delete by index
// void removeByIndex(struct Node **, int idx);

// get rid of entire list
void clearList(struct Node **);

// find a value in the list, return pointer or NULL
// struct Node* findByVal(struct Node *, int val);
// struct Node* findByIdx(struct Node *, int idx);

/* Sorted Int List Interface */

// For a sorted list, we can't just insert anywhere
void insertSorted(struct Node **, int val);
// For a sorted list, this is much faster
void removeSorted(struct Node **, int val);
// Sorted search is much faster
// struct Node* findByValSorted(struct Node*, int val);
#endif
