#include <stdlib.h>
#include "list.h"

// Recursive version is often simpler
int size(struct Node* node) {
    if (node == NULL) return 0;
    return (1 + size(node->next));
}

// Iterative version
// int size(struct Node *node) { }
