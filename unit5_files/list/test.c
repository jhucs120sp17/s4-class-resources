#include <stdio.h>
#include "list.h"

void expectTest(int i, char* s, struct Node* h) {
    printf("Test %d\n", i);
    printf("Expected: %s\n", s);
    printf("Found   :");
    printList(h);
    puts("");
}

void testList() {
  struct Node* head = NULL;
  int i=1;
  pushFront(&head, 50);
  pushFront(&head, 10);
  pushFront(&head, 2);
  expectTest(i++, "[2, 10, 50]", head);

  pushBack(&head, 100);
  pushBack(&head, 250);
  expectTest(i++, "[2, 10, 50, 100, 250]", head);

  insertSorted(&head, 5);
  insertSorted(&head, 1);
  expectTest(i++, "[1, 2, 5, 10, 50, 100, 250]", head);

  insertSorted(&head, 300);
  expectTest(i++, "[1, 2, 5, 10, 50, 100, 250, 300]", head);

  removeSorted(&head, 10);
  removeSorted(&head, 300);
  removeSorted(&head, 100);
  removeSorted(&head, 7);
  expectTest(i++, "[1, 2, 5, 50, 250]", head);
  
  removeSorted(&head, 1);
  removeSorted(&head, 2);
  removeSorted(&head, 5);
  removeSorted(&head, 50);
  removeSorted(&head, 250);
  expectTest(i++, "[]", head);

  insertSorted(&head, 45);
  insertSorted(&head, 30);
  expectTest(i++, "[30, 45]", head);

  printf("size is %d\n", size(head));

  clearList(&head);
  expectTest(i++, "[]", head);
}
int main() {
  testList();
}
