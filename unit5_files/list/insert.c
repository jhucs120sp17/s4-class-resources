#include <stdlib.h>
#include "list.h"

// Assumes a sorted list
void insertSorted(struct Node** head, int val) {
  struct Node* p = *head;
  struct Node* q = p;
  while(p != NULL && p->data < val) {
    q = p;
    p = p->next;
  }

  // Allocation & fill in
  struct Node* elem = malloc(sizeof(struct Node));
  elem->data = val;

  // Handle all cases
  if (p == *head) {
    // First element or empty list
    // Implement this
  } else {
    // We're in the middle or end
    // Implement this
  }
}
