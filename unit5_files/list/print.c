#include <stdio.h>
#include <stdbool.h>
#include "list.h"

void printList(struct Node* node) {
    printf("[");
    bool first = true;
    while(node != NULL) {
        printf("%s%d",
            first ? "" : " ,",
            node->data);
        node = node->next;
        first = false;
    }
    printf("]");
}
