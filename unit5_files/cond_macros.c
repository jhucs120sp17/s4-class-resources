#include <stdio.h>

int main() {
    int x = 10;
#if DEBUG
    // How do we get this working?
    printf("x=%d\n", x);
#elif DEBUG == 2
    printf("x is at %p\n", (void *)&x);
#else
    puts("No go!");
#endif
    return 0;
}
