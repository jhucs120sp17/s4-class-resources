#include <stdio.h>
#include <stdlib.h>
// Typedef in one statement
typedef struct Name {
    char *first;
    char *last;
} Name;

// Or separately
struct Person {
    Name name; // Woah!
    int age;
};

typedef struct Person Person;

void printPerson(Person *p) {
    printf("%s %s is %d years old\n", p->name.first, p->name.last, p->age);
}
int main() {
    // Nested initialization
    Person people[] = {{{"John", "Smith"}, 25}, {{"Bob", "Dole"}, 75}};
    // Print the details
    for (int i=0; i<4; i++) {
        printPerson(&(people[i]));
    }
}
