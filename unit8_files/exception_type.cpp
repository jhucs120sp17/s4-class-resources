#include <iostream>
#include <fstream>
#include <string>
#include <exception>

void foo() throw (std::runtime_error) {
    std::ifstream strm("file.txt");
    if (!strm.is_open()) {
        throw std::runtime_error("Failed to open file");
    }
}

void bar() {
    foo();
}

int main() {
    try {
        // Only stuff in here is caught
        bar();
    }
    catch (std::exception &e) {
        std::cout << "Exception thrown: "
        // std::exception's what() describes them
            << e.what() << std::endl;
    }
    // Normal execution
    std::cout << "file is open!\n";
}
