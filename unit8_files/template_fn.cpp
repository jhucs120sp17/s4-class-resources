#include <iostream>

template <typename T>
T foo(const T& x) { return x; }

template <int I>
void bar() {
  std::cout << foo(I) << std::endl;
}

int main() {
    bar<10>();
}
