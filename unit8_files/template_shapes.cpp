#include <iostream>
#include <string>

// Interface without virtual functions!
// (Compare to unit 7)
class Shape {
public:
    double area()=0;
    std::string name()=0;
};
class Square {
public:
    Square(double x) : m_len(x) {}
    double area() {
        return m_len * m_len; }
    std::string name() { return "Square"; }
protected:
    double m_len = 0;
};
class Circle: public Shape {
public:
    Circle(double x) : m_radius(x) {}
    double area() {
        return m_radius * m_radius * 3.141; }
    std::string name() { return "Circle"; }
protected:
    double m_radius = 0;
};
// Make foo work with any shape
void foo(Shape& s) {
    std::cout << "The " <<
        s.name() << "has area "
        << s.area() << "\n";
}
int main() {
    Square s(4);
    Circle* c = new Circle(15);
    foo(sh);
    foo(s);
    foo(*c);
    delete c;
};
