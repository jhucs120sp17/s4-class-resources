#include <iostream>
#include <list>
#define JUICE 1
#define APPLE 2
#define BANANA 3

class ShoppingList {
public:
    using li = std::list<int>;
    void insert(int item) { m_list.push_front(item); }
    void print() {
        li::iterator it = m_list.begin();
        for (; it != m_list.end(); ++it) {
            switch (*it) {
                case JUICE: std::cout << "juice ";
                break;
                case APPLE: std::cout << "apple ";
                break;
                case BANANA: std::cout << "banana ";
                break;
            }
            std::cout << "\n";
        }
    }
private:
    std::list<int> m_list;
};

int main() {
    // Use C++ enums to fix this problem
    ShoppingList l;
    l.insert(APPLE);
    l.insert(BANANA);
    l.insert(40);
    l.print();
}
