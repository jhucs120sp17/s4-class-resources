#include <cstdio>

class IntVector {
public:
    class iterator {
    public:
        iterator(int *ptr) { m_ptr = ptr; }
        int operator*() {
            return *m_ptr;
        }
        iterator& operator++() {
            m_ptr++;
            return *this;
        }
        iterator operator++(int) {
            iterator temp = *this;
            m_ptr++;
            return temp;
        }
        bool operator==(iterator& other) {
            return m_ptr == other.m_ptr;
        }
        int *m_ptr = nullptr;
    };
    IntVector() : m_size(0), m_capacity(0), m_array(nullptr) { }
    ~IntVector() {
        delete [] m_array;
    }
    IntVector(const IntVector& v) {
        m_size = v.m_size;
        m_capacity = v.m_capacity;
        m_array = new int[m_capacity];
        for (int i=0; i<m_size; i++) {
            m_array[i] = v.m_array[i];
        }
    }
    IntVector& operator=(const IntVector& v) {
        m_size = v.m_size;
        m_capacity = v.m_capacity;
        m_array = new int[m_capacity];
        for (int i=0; i<m_size; i++) {
            m_array[i] = v.m_array[i];
        }
        return *this;
    }
    void push_back(int i) {
        if (m_size >= m_capacity) {
            // allocate
            m_capacity = m_capacity ? m_capacity * 2 : 2;
            int *temp = new int[m_capacity];
            for (int i=0; i<m_size; i++) {
                temp[i] = m_array[i];
            }
            delete [] m_array;
            m_array = temp;
        }
        m_array[m_size++] = i;
    }
    int size() {
        return m_size;
    }
private:
    int m_capacity;
    int m_size;
    int* m_array;
};

int main() {
    IntVector v;
    v.push_back(3);
    v.push_back(5);
    IntVector u;
    u = v;
    u.push_back(10);
    printf("%d\n", v.size());
    return 0;
}
