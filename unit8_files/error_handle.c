#include <stdio.h>
#include <stdlib.h>
int foo() {
    FILE *fp; char *p;
    if (!(fp = fopen("file.txt", "r")))
        return -1;
    if (!(p = malloc(1000)))
        return -2;
    return 0;
}

int bar() {
    printf("calling foo\n");
    return foo();
}

int main() {
    int ret = foo();
    if (ret == -1) {
        puts("file error in foo!");
        return -1;
    } else if (ret == -2) {
        puts("memory error in foo!");
        return -2;
    }
    return 0;
}
