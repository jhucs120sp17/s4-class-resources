#include <vector>
#include <iostream>
#include <cassert>
using namespace std;

template <typename T>
class Vector {
public:
  T dotProduct(Vector<T> &other);
  Vector(vector<T> &v) : m_vector(v) {}
private:
  // We need a separate type variable for friend
  template <typename U>
  friend std::ostream& operator<<(std::ostream&, Vector<U>);
  vector<T> m_vector;
};

// Example of how to declare the method outside the class declaration
template <typename T>
T Vector<T>::dotProduct(Vector &other) {
  // Make dotProduct take 'any' type
  assert(m_vector.size() == other.m_vector.size());
  T total = 0;
  for (size_t i=0; i<other.m_vector.size(); i++) {
    total += m_vector[i] * other.m_vector[i];
  }
  return total;
}
template <typename T>
std::ostream& operator<<(std::ostream& os, Vector<T>& v) {
    typename vector<T>::iterator it;
    for (it = v.m_vector.begin();
         it != v.m_vector.end(); it++) {
         os << *it << ' ';
    }
    os << '\n';
}

int main() {
    vector<int> u = {1, 2, 3, 4, 5};
    vector<int> u2 = {3, 4, 5, 6, 7};
    Vector<int> v(u); 
    Vector<int> v2(u2); 
    cout << v.dotProduct(v2);
}
