#include <vector>
#include <string>

template <typename T>
class Array {
    void insert(T val) {
        m_vector.push_back(val);
    }
    std::vector<T> m_vector;
};

// Note this syntax for specialization
template <>
class Array<char> {
  // Create the class for holding chars in a string
};
int main() {
    Array<int> ivec;
    Array<char> cvec;
    ivec.insert(4);
    cvec.insert('r');
}
