#include <iostream>
#include <fstream>
#include <string>
#include <exception>

void foo() {
    std::ifstream strm("file.txt");
    if (!strm.is_open()) {
        // throw an int, then change to
        // a standard exception
    }
}

void bar() {
    foo();
}

int main() {
    // handle the exception
    bar();
    // Normal execution
    std::cout << "file is open!\n";
}
