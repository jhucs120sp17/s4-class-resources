#include <iostream>
#include <string>

// Interface without virtual functions!
class Square {
public:
    Square(double x) : m_len(x) {}
    double area() {
        return m_len * m_len; }
    std::string name() { return "Square"; }
protected:
    double m_len = 0;
};
class Circle {
    Circle(double x) : m_radius(x) {}
    double area() {
        return m_radius * m_radius * 3.141; }
    std::string name() { return "Circle"; }
protected:
    double m_radius = 0;
};
template <typename SHAPE>
void foo(SHAPE& s) {
    std::cout << "The " << s.name()
    << "has area " << s.area() << "\n";
}
int main() {
    Square s[10];
    s[4] = Circle(3);
    for (int i=0; i<10; i++) {
        foo(s)
    }
};
