// Fix this code
#include <list>
#include <string>
#include <iostream>

template <typename T>
T sum(list<T> l, T zero) {
    T total = zero;
    for (std::list::iterator i=l.begin(); i != l.end(); ++i) {
       T += i;
    }
}

int main() {
    list<int> li = {1, 2, 3, 4};
    std::list<std::string> ls = {"hello ", "there ", "world"};
    // call sum for both lists
    int i;
    string s;
    std::cout << i << ' ' << s << '\n';
}
