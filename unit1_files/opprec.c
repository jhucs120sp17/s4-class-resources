#include <stdio.h>

int main() {
    int fahrenheit = 75;
    float celsius = 5.0 / 9.0 *
      fahrenheit - 32;
    // print to 2 decimal places
    printf("%0.2f", celsius);
    return 0;
}
