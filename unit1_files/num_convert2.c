#include <stdio.h>

int main() {
    int fahrenheit = 75;
    float celsius = 5 / 9 *
      (fahrenheit - 32);
    // print to 2 decimal places
    printf("%.2f", celsius);
    return 0;
}
