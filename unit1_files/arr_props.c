#include <stdio.h>
void foo(int arr[]) {
  for (int i=0; i<10; i++) {
    if (arr[i] % 7 == 0) {
      break;
    } else if (arr[i] % 2 == 0) {
      printf("even ");
    } else if (arr[i] % 3 == 0) {
      printf("three ");
      continue;
    }
    printf("%d ", arr[i]);
  }
  printf("\n");
}
int main() {
  int ra[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  foo(ra);
  return 0;
}
