#include <stdio.h>
int main() {
    float f = 4.3562;
    int i = -4000000;
    printf("%d\n", f); // what happens here?
    printf("%d\n", (int)f); // how about here?
    printf("%f\n", (double)f); // and here?
    printf("%c\n", (char)f); // ...?
    // print i as an unsigned int, char, and double
    return 0;
}
