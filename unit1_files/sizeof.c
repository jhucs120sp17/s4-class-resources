#include <stdio.h>
int main() {
  const int x = 0; // just showing const
  const float y = 3.14;
  double z;
  long long int u;
  char c;

  // Notice 2 ways of multiline string literals
  printf("size of:\nint: %lu\nfloat: %lu\n"
   "double: %lu\nlong long: %lu\n \
   char: %lu\n",
    // Notice how we can use both variables
    // and types
    sizeof(int), sizeof(y), sizeof(z),
    sizeof(u), sizeof(c));
   return 0;
}
