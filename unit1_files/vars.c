#include <stdio.h>
int main() {
    int x=2, y=10; // add z = 3
    printf("Average is %f\n", (x + y / 2.0));
    printf("Variance is %f\n",
      ((x - (x + y / 2.0)) * (x - (x + y / 2.0)) +
      (y - (x + y / 2.0)) * (y - (x + y / 2.0))) / 2.0);
    return 0;
}
