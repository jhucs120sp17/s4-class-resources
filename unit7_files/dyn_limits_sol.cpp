#include <iostream>

class Foo {
public:
    virtual void foo() {
        std::cout << "Foo::foo:" << get_mx() << '\n';
        bar();
    }
    virtual void bar() {}
    virtual int get_mx() { return 0; }
};
class Bar : public Foo {
public:
    virtual void bar() {
        std::cout << "Bar::bar:" << get_mx() << '\n';
    }
    virtual int get_mx() { return m_x; }
    int m_x = 5;
};
void baz(Foo& f) {
    f.foo();
    f.bar();
}
int main() {
    Bar b;
    baz(b);
}
