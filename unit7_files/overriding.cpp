#include <iostream>
struct Foo {
  void foo() {
      std::cout << "Foo::foo()\n"; }
  void foofoo() {
      std::cout << "Foo::foofoo()\n"; }
};
struct Bar : public Foo {
  // we automatically share Foo::foofoo()
  // overriding Foo::foo
  void foo() {
      std::cout << "Bar::foo()\n"; }
  // extending Foo
  void bar() {
      std::cout << "Bar::bar()\n"; }
};
int main() {
  // Invoke Foo::foo()
  // Invoke Bar::foo()
  // Invoke Foo::foofoo() from Bar
  // Invoke Bar::bar()
}

