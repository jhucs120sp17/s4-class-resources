#include <iostream>

class Drawable {
public:
    Drawable(char s): m_symbol(s) {}
    virtual void draw() { std::cout << "###"; }
protected:
    char m_symbol;
};
class DrawableShape : public Drawable {
public:
    DrawableShape(char s): Drawable(s) {}
    virtual void foo() {}
};
class DrawableEllipse : public Drawable {
public:
    DrawableEllipse(char s): Drawable(s) {}
    virtual void bar() {}
};
class Circle: public DrawableShape, DrawableEllipse {
public:
    Circle(int x, char c) :
        m_x(x), DrawableShape(c), DrawableEllipse(c) {}
    virtual double area() {
        return m_x * m_x * 3.141; }
protected:
    int m_x;
};
void foo(Circle& s) {
    s.draw();
}
int main() {
    Circle c(2, '#');
    foo(c);
}
