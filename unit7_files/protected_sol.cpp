#include <iostream>
class Person {
protected:
    string name;
    string ssn;
};
class Employee : public Person {
public:
    void foo() {
        std::cout << name;
        std::cout << ssn;
    }
};
