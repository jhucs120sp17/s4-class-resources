#include <iostream>
#include <string>
// Abstract Shape class
class Shape {
public:
   // Note this
   virtual double area()=0;
   // And this
   virtual std::string name() {
       return "Shape";
   }
   // NOTE!
   virtual ~Shape() {}
};
class Square: public Shape {
   // Fill this in
};
class Circle: public Shape {
   // Fill this in
};
void foo(Shape& s) {
    std::cout << "The " << s.name()
    << "has area " << s.area() << "\n";
}
int main() {
    Square s(4);
    Circle* c = new Circle(15);
    Shape sh;
    foo(sh);
    foo(s);
    foo(*c);
    delete c;
};
