// Fix this
#include <iostream>
#include "draw.h";
class Shape {
public:
    Shape(int x): m_x(x) {}
    virtual double area()=0;
protected:
    int m_x;
};
class Drawable {
public:
    Drawable(char s): m_symbol(s) {}
    virtual void draw()=0;
protected:
    char m_symbol;
};
class Square: public Shape, public Drawable {
public:
    Square(int x) : Shape(x), Drawable('.') {}
    virtual double area() {
        return m_x * m_x; }
    virtual void draw() {
        Draw::rect(m_x, m_x, m_symbol);
    }
};
class Circle: public Shape, public Drawable {
public:
    Circle(double x, char c) :
        Shape(x), Drawable(c) {}
    virtual double area() {
        return m_x * m_x * 3.141; }
    virtual void draw() {
        Draw::circle(m_x, m_symbol);
    }
};
void foo(Drawable& s) {
    std::cout << s.area() << '\n';
    s.draw();
}
int main() {
    Square s(4);
    Circle c(2, '#');
    foo(s);
    foo(c);
};
