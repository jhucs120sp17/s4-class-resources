#include <iostream>
#include <vector>
#include <string>
#include "shapes.h"

void foo(Shape& s) {
    std::cout << "The " << s.name()
    << "has area " << s.area() << "\n";
}

class Rectangle: public Square {
public:
    Rectangle(double l, double w) : Square(l), m_width(w) {}
    virtual double area() { return m_len * m_width; }
    virtual std::string name() { return "Rectangle"; }
protected:
    double m_width = 0;
};

int main() {
    using vc = std::vector<Shape *>;
    vc v;
    for (int i=0; i<5; i++) v.push_back(new Circle(i+1));
    for (int i=0; i<3; i++) v.push_back(new Square(i+1));
    for (int i=0; i<2; i++) v.push_back(new Rectangle(i+1, i+2));
    for (unsigned int i=0; i<v.size(); i++) foo(*(v[i]));
}
