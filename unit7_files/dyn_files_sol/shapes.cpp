#include "shapes.h"

double Square::area() {
    return m_len * m_len;
}

std::string Square::name() { return "Square"; }

double Circle::area() {
    return m_radius * m_radius * 3.141;
}
std::string Circle::name() { return "Circle"; }
