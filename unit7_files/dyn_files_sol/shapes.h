#ifndef SHAPES_H
#define SHAPES_H
#include <string>
class Shape {
public:
    virtual double area()=0;
    virtual std::string name() {
        return "Shape";
    }
};
class Square: public Shape {
public:
    Square(double x) : m_len(x) {}
    virtual double area();
    virtual std::string name();
protected:
    double m_len = 0;
};
class Circle: public Shape {
public:
    Circle(double x) : m_radius(x) {}
    virtual double area();
    virtual std::string name();
protected:
    double m_radius = 0;
};
#endif
