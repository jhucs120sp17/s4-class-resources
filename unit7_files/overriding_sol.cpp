#include <iostream>
struct Foo {
  void foo() {
      std::cout << "Foo::foo()\n"; }
  void foofoo() {
      std::cout << "Foo::foofoo()\n"; }
};
struct Bar : public Foo {
  // we automatically share Foo::foofoo()
  // overriding Foo::foo
  void foo() {
      std::cout << "Bar::foo()\n"; }
  // extending Foo
  void bar() {
      std::cout << "Bar::bar()\n"; }
};
int main() {
  Foo f; Bar b;
  Foo* pf = &b; Bar& rb = b;
  f.foo(); pf->foo(); // Invoke Foo::foo()
  // Invoke Bar::foo()
  b.foo(); rb.foo();
  // Invoke Foo::foofoo() from Bar
  b.foofoo(); pf->foofoo(); rb.foofoo();
  // Invoke Bar::bar()
  b.bar(); rb.bar();
}

