class Window {
public:
    void open() { m_open = true; }
    void close() { m_open = false; }
private:
    bool m_open;
};
class House {
public:
    // Notice these
    void openWindow() { m_win.open(); }
    void closeWindow() { m_win.close(); }
private:
    Window m_win;
};
int main() {
    House h;
    h.open(); //?
    h.close(); //?
}
