#include <iostream>
#include <vector>
#include <string>
#include "shapes.h"

void foo(Shape& s) {
    std::cout << "The " << s.name()
    << "has area " << s.area() << "\n";
}

int main() {
    using vc = std::vector<Shape *>;
    vc v;
    for (int i=0; i<5; i++) v.push_back(new Circle(i+1));
    for (int i=0; i<3; i++) v.push_back(new Square(i+1));

    // Add a new subclass of Square, Rectangle, and add 2
    // of them to the vector, without touching the same files

    for (unsigned int i=0; i<v.size(); i++) foo(*(v[i]));
}
