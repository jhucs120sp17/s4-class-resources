#include <iostream>
// Fix this

class Foo {
public:
    virtual void foo() {
        std::cout << "Foo::foo:" << m_x << '\n';
        bar();
    }
};
class Bar : public Foo {
public:
    virtual void bar() {
        std::cout << "Bar::bar:" << m_x << '\n';
    }
    int m_x = 5;
};
void baz(Foo& f) {
    f.foo();
    f.bar();
}
int main() {
    Bar b;
    baz(b);
}
