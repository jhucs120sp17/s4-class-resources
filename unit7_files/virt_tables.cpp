#include <iostream>

// Inspect with GDB
class Foo {
public:
    virtual void foo() { std::cout << m_x << '\n'; }
    void bar() { std::cout << "Foo::bar\n"; }
    int m_x = 3;
};
class Bar : public Foo {
public:
    virtual void bar() { std::cout << m_y << '\n'; }
    int m_y = 5;
};
class Baz : public Bar {
public:
    virtual void bar() override { std::cout << m_z << '\n'; }
    int m_z = 7;
};
int main() {
    Foo f; Bar b; Baz z;
    Foo &rf = f; Foo &rb = b; Foo &rz = z; 
    rf.foo();
    rf.bar();
    rb.foo();
    rb.bar();
    rz.foo();
    rz.bar();
}
