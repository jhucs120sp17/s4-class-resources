#include <iostream>
using namespace std;
class Foo {
public:
    void foo() { cout << "foo" << endl; }
};
class Bar : public Foo {
public:
    void bar() { foo(); }
};
int main() {
    Foo f;
    f.foo(); //?
    f.bar(); //?
    Bar b;
    b.foo(); //?
    b.bar(); //?
}
