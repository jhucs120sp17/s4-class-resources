#define CATCH_CONFIG_MAIN
#include "catch.h"
#include <stdio.h>
#include <string.h>

TEST_CASE("string test", "[strings]") {
    char s1[] = "hello ";
    char s2[] = "world";
    char s3[50];

    REQUIRE(sizeof(s1) + sizeof(s2) < sizeof(s3));

    SECTION("concatenating strings") {
        strcat(s3, s1);
        strcat(s3, s2);
        REQUIRE(strlen(s3) ==
            strlen(s1) + strlen(s2));

        // This follows the above
        SECTION("comparing to complete"
            "string should work") {
            REQUIRE(strcmp(s3, "hello world") == 0);
        }
        SECTION("should be same as "
            "copying strings") {
            char s4[50];
            strcpy(s4, s1);
            strcpy(s4+strlen(s1), s2);

            REQUIRE(strlen(s4) ==
                strlen(s1) + strlen(s2));
            REQUIRE(strcmp(s3, s4) == 0);
        }
    }
}
