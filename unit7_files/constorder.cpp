#include <iostream>
class Foo {
public:
    Foo(int x) {
      std::cout << "Foo: constr\n"; }
    ~Foo() {
      std::cout << "Foo: destr\n"; }
};
class Bar : public Foo {
public:
    Bar() : Foo(10) {
      std::cout << "Bar: constr\n"; }
    ~Bar() {
      std::cout << "Bar: destr\n"; }
};
class Baz : public Bar {
public:
    // Notice: no list
    Baz(float y) {
      std::cout << "Baz: constr\n"; }
    ~Baz() {
      std::cout << "Baz: destr\n"; }
};
int main() { Baz(10.5); }
