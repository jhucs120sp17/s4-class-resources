#ifndef DRAW_H
#define DRAW_H
#include <math.h>
#include <iostream>

class Draw {
public:
    static void circle(int r, char c) {
        for (int j=-r; j<=r; j++) {
            for (int i=-r; i<=r; i++) {
               if (i*i + j*j <= r*r) std::cout << c;
               else std::cout << ' ';
            }
            std::cout << '\n';
        }
        std::cout << '\n';
    }
    static void rect(int l, int w, char c) {
        for (int j=0; j<w; j++) {
            for (int i=0; i<l; i++) {
                std::cout << c;
            }
        }
    }
};
#endif
