#include <iostream>
class Shape {
public:
    Shape(int x): m_x(x) {}
    virtual double area()=0;
    virtual void draw() { std::cout << "@@@"; }
protected:
    int m_x;
};
class Drawable {
public:
    Drawable(char s): m_symbol(s) {}
    virtual void draw() { std::cout << "###"; }
protected:
    char m_symbol;
};
class Circle: public Shape, public Drawable {
public:
    Circle(double x, char c) :
        Shape(x), Drawable(c) {}
    virtual double area() {
        return m_x * m_x * 3.141; }
};
void foo(Circle& s) {
    s.Drawable::draw(); // Was ambiguous before
}
int main() {
    Circle c(2, '#');
    foo(c);
};
