#include <iostream>
using namespace std;
class Foo {
public:
    void foo() { cout << "foo" << endl; }
};
class Bar : public Foo {
public:
    void bar() { foo(); }
};
int main() {
    Foo f;
    f.foo(); // works
    //f.bar(); // doesn't
    Bar b;
    b.foo(); // works
    b.bar(); // works
}
