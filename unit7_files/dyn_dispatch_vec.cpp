#include <iostream>
#include <string>
// Abstract Shape class
class Shape {
public:
    virtual double area()=0;
    virtual std::string name() {
        return "Shape";
    }
    virtual ~Shape();
};
class Square: public Shape {
public:
    Square(double x) : m_len(x) {}
    virtual double area() {
        return m_len * m_len; }
    virtual std::string name() {
        return "Square"; }
protected:
    double m_len = 0;
};
class Circle: public Shape {
public:
    Circle(double x) : m_radius(x) {}
    virtual double area() {
        return m_radius * m_radius * 3.141; }
    virtual std::string name() {
        return "Circle"; }
protected:
    double m_radius = 0;
};
void foo(Shape& s) {
    std::cout << "The " <<
        s.name() << "has area "
        << s.area() << "\n";
}
int main() {
    // Create a vector of 5 Circles of increasing radii

    // Add 3 squares of increasing lengths to the vector

    // Run foo on every member of the vector
};
