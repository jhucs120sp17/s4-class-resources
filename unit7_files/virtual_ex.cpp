#include <iostream>
class Foo {
public:
    virtual void foo() { std::cout << "Foo::foo\n"; }
    void bar() { std::cout << "Foo::bar\n"; }
};
class Bar : public Foo {
public:
    virtual void foo() override { std::cout << "Bar::foo\n"; }
    virtual void bar() { std::cout << "Bar::bar\n"; }
};
class Baz : public Bar {
public:
    void bar() { std::cout << "Baz::bar\n"; }
};
int main() {
    Foo f; Bar b; Baz z;
    Foo *pf = &z; Bar &rb = z; Baz &rz = z;
    // Try to activate each function
}
